package leohuang.snake;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.event.ActionEvent;


public class Main extends javafx.application.Application {

    final double frameRate = 30.0;
    Pane splashScreen = new Pane();
    Pane endScreen = new Pane();
    StackPane levelScreen = new StackPane();
    Scene scene;

    public static void main(String[] args)
    {
        Application.launch(args);
    }

    //load the app window with the starting screen
    @Override
    public void start(Stage stage){
        //set up the game by loading the splash screen
        startSplash();
        scene = new Scene(splashScreen, 750, 825);
        splashScreen.requestFocus();
        stage.setScene(scene);
        stage.setResizable(false);
        stage.setTitle("Leo's Snake Game");
        stage.show();
        stage.setOnCloseRequest(e -> {System.exit(0);});
    }

    //set up the starting screen with its fxml and event receiver
    public void startSplash() {
        //splash screen
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/startscreen.fxml"));
            splashScreen = fxmlLoader.load();
            splashScreen.addEventFilter(KeyEvent.KEY_PRESSED, e -> {
                if (e.getCode() == KeyCode.Q) {
                    System.exit(0);
                } else if (e.getCode() == KeyCode.DIGIT1) {
                    startLevel(1);
                } else if (e.getCode() == KeyCode.DIGIT2) {
                    startLevel(2);
                } else if (e.getCode() == KeyCode.DIGIT3) {
                    startLevel(3);
                }
                levelScreen.requestFocus();
            });
            splashScreen.requestFocus();
        } catch (Exception e) {
            System.err.println(e);
            System.exit(1);
        }
    }

    //change the root of the scene to the desired game screen
    public void switchScreen(Timeline animLoop, Timeline gameLoop, Pane root) {
        animLoop.stop();
        gameLoop.stop();
        scene.setRoot(root);
        root.requestFocus();
    }

    //set up the elements of the game, including all game objects and timers to move animation and gameplay
    //Note that the fxml reader that opens endscreen.fxml will output garbage lines whenever it is instantiated
    //  one line for each imageview on the UI (maybe there's something weird with the images?)
    public void startLevel(int level){
        GameStage levelStage = new GameStage(level);
        levelScreen = new StackPane();
        levelScreen.getChildren().add(levelStage);
        levelScreen.setAlignment(levelStage, Pos.BOTTOM_CENTER);
        //loop for animating the game
        Timeline animLoop = new Timeline();
        animLoop.setCycleCount( Timeline.INDEFINITE );
        //make the game draw itself every frame
        KeyFrame akf = new KeyFrame(Duration.seconds(1.0/frameRate),
                new EventHandler<ActionEvent>()
                {
                    public void handle(ActionEvent e)
                    {
                        levelStage.draw();
                    }
                });
        animLoop.getKeyFrames().add(akf);
        animLoop.play();
        //loop for updating gamestate
        Timeline gameLoop = new Timeline();
        gameLoop.setCycleCount( Timeline.INDEFINITE );
        //update at the same rate that the snake moves
        KeyFrame gkf = new KeyFrame(Duration.seconds(levelStage.snake.speed),
                new EventHandler<ActionEvent>()
                {
                    public void handle(ActionEvent e)
                    {
                        //this boolean checks if the game has ended (meaning the player has died)
                        boolean end = levelStage.updateGameState();
                        if (end) {
                            try {
                                //show the snake colliding with an object
                                levelStage.draw();
                                //stop game
                                animLoop.stop();
                                gameLoop.stop();
                                //load end screen
                                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/endscreen.fxml"));
                                endScreen = fxmlLoader.load();
                                //no need to add event handling since levelscreen will handle them for us
                                //rather than changing the root of the scene, we add this pane to the children of levelscreen
                                //this way we have the end screen overlaid on top of the game and the player can see both
                                levelScreen.getChildren().add(endScreen);
                                levelScreen.setAlignment(endScreen, Pos.CENTER);
                            } catch (Exception ex) {
                                System.err.println(ex);
                                System.exit(1);
                            }

                        }
                    }
                });
        gameLoop.getKeyFrames().add(gkf);
        gameLoop.play();
        //user can press a key at any point to leave the level
        levelScreen.addEventFilter(KeyEvent.KEY_PRESSED, e -> {
            //reset
            if (e.getCode() == KeyCode.R) {
                    switchScreen(animLoop, gameLoop, splashScreen);
                    startSplash();
            //quit
            } else if (e.getCode() == KeyCode.Q) {
                System.exit(0);
            //level 1
            } else if (e.getCode() == KeyCode.DIGIT1) {
                startLevel(1);
                switchScreen(animLoop, gameLoop, levelScreen);
            //level 2
            } else if (e.getCode() == KeyCode.DIGIT2) {
                startLevel(2);
                switchScreen(animLoop, gameLoop, levelScreen);
            //level 3
            } else if (e.getCode() == KeyCode.DIGIT3) {
                startLevel(3);
                switchScreen(animLoop, gameLoop, levelScreen);
            //the game object will handle control input
            } else {
                levelStage.takeKeyInput(e);
            }
        });
        scene.setRoot(levelScreen);
    }
}
