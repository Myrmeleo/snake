package leohuang.snake;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeLineCap;

//the snake that the player will control
//represented as a linked list
public class Snake{
    //4 possible directions of movement
    public enum Direction{North, East, West, South}

    static final int squareWidth = 50;

    //segments of snake are represented as nodes
    public class SnakePart {
        //x-y coordinates are expressed in terms of the square on the screen they are at
        //measured from the top left corner starting at (0,0)
        public int x;
        public int y;
        //point to the nodes it's linked to (previous means closer to the tail, next means closer to the head)
        SnakePart previous, next;
        //head will have special implementation
        boolean isHead;
        //new nodes will freeze for one movement cycle due to being instantiated at the same coordinates as the tail node
        boolean isNew;

        //param is the new x and y coordinates
        SnakePart(int sx, int sy) {
            x = sx;
            y = sy;
            previous = null;
            next = null;
            isHead = false;
            isNew = true;
        }

        //copy constructor making it so that the new instance will take the place of sp
        //this means that next will be sp and previous will be sp.previous
        SnakePart(SnakePart sp) {
            x = sp.x;
            y = sp.y;
            previous = sp.previous;
            next  = sp;
            sp.previous = this;
            isHead = false;
            isNew = true;
        }

        //moving the snake means that each node will take the place of the next node
        //param is the direction the snake is moving - not used unless the node is the head
        void move(Direction d) {
            //new nodes freeze for one movement cycle because they are instantiated at the coordinates of the following node
            //this lets the rest of the snake move one square so the new node does not overlap
            if (isNew) {
                isNew = false;
            } else {
                if (isHead) {
                    if (d == Direction.North) {
                        y -= 1;
                    } else if (d == Direction.South) {
                        y += 1;
                    } else if (d == Direction.East) {
                        x += 1;
                    } else {
                        x -= 1;
                    }
                } else {
                    x = next.x;
                    y = next.y;
                }
            }
            //call the next node to move
            if (next != null) {
                next.move(d);
            }
        }

        void setToHead() {
            isHead = true;
        }

        void setPrevious(SnakePart sp) {
            previous = sp;
        }

        void setNext(SnakePart sp) {
            next = sp;
        }

        //check if the snake ran into itself
        //do this by checking for overlapping coordinates
        boolean checkCollision(SnakePart other) {
            return (x == other.x && y == other.y);
        }

        boolean checkCollision(GameStage.Wall w) {return (w.topLeftX <= x && w.topLeftY <= y &&
                w.topLeftX + w.width - 1 >= x && w.topLeftY + w.height - 1 >= y);};

        boolean checkCollision(GameStage.Fruit f) {return (x == f.x && y == f.y);}

        //draw the snake part (basically a line from this node to the previous
        void draw(GraphicsContext gc) {
            if (previous != null) {
                double widthToRestore = gc.getLineWidth();
                gc.setLineWidth(squareWidth * 2 / 3);
                gc.setStroke(Color.RED);
                gc.setLineCap(StrokeLineCap.ROUND);
                gc.strokeLine(squareWidth / 2 + x * squareWidth, squareWidth / 2 + y * squareWidth,
                        squareWidth / 2 + previous.x * squareWidth, squareWidth / 2 + previous.y * squareWidth);
                previous.draw(gc);
                gc.setLineWidth(widthToRestore);
            }
        }
    }

    //head and tail nodes will handle all functions the whole snake needs
    public SnakePart head;
    public SnakePart tail;
    public int length;
    public Direction dir;
    //how much time it takes to move one square
    public double speed;

    public Snake() {
        head = new SnakePart(3, 7);
        head.setToHead();
        tail = new SnakePart(2, 7);
        head.setPrevious(tail);
        tail.setNext(head);
        dir = Direction.East;
        length = 2;
        speed = 0.15;
    }

    //grow the snake
    public void addPart() {
        SnakePart newTail = new SnakePart(tail);
        tail = newTail;
        length += 1;
    }

    public void move() {
        tail.move(dir);
    }

    //check if the snake has run into itself
    //do this by checking if the head ran into any of the other parts
    public boolean checkSelfCollision() {
        boolean collided = false;
        SnakePart nextToCheck = head.previous;
        while (!collided && nextToCheck != null) {
            collided = head.checkCollision(nextToCheck);
            nextToCheck = nextToCheck.previous;
        }
        return collided;
    }

    public boolean checkCollision(GameStage.Wall w) {
        //because it's possible for the snake to spawn on top of a wall when going to a new level
        //we must check collision with all segments
        boolean collided = false;
        SnakePart nextToCheck = head;
        while (!collided && nextToCheck != null) {
            collided = nextToCheck.checkCollision(w);
            nextToCheck = nextToCheck.previous;
        }
        return collided;
    }

    public void draw(GraphicsContext gc) {
        head.draw(gc);
    }

    public int getX() {return head.x;}
    public int getY() {return head.y;}
}
