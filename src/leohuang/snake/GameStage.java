package leohuang.snake;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import java.util.ArrayList;


//this class is not used for game objects since an x and y field will work just fine
//however, to keep track of unoccupied tiles a class for coords is convenient
class Coords {
    int x;
    int y;
    public Coords(int sx, int sy) {
        x = sx;
        y = sy;
    }
    public boolean equals(Coords other) {
        return (x == other.x && y == other.y);
    }
}

//the level for the snake to move in, including fruits, walls, powerups (if I get to that)
public class GameStage extends Canvas {
    //stage will be 15x15 squares, 750x750 pixels
    //coordinates are measured from top left starting at (0,0)
    static final int squareWidth = 50;
    static final int rowCount = 15;

    //represents the physical obstacles that the snake cannot run into
    public class Wall {
        //dimensions and collision detection can be calculated using the top left corner and width and height
        //because the Wall is always rectangular
        public int topLeftX;
        public int topLeftY;
        public int width;
        public int height;
        //if a wall spawns on top of the snake, don't put it in the level
        boolean active;

        //param is the coords of the top left corner, plus the width and height
        //w and h are given in terms of squares on the board, not in pixels
        Wall(int tlX, int tlY, int w, int h) {
            topLeftX = tlX;
            topLeftY = tlY;
            width = w;
            height = h;
            active = true;
        }

        boolean isActive() {return active;}

        void draw(GraphicsContext gc) {
            gc.setFill(Color.MEDIUMPURPLE);
            gc.fillRoundRect(topLeftX * squareWidth, topLeftY * squareWidth, width * squareWidth, height * squareWidth, 35, 35);
        }
    }

    //the fruit the snake must eat
    public class Fruit {
        public int x;
        public int y;

        //params are in terms of squares on the board
        Fruit(int sx, int sy) {
            x = sx;
            y = sy;
        }

        void draw(GraphicsContext gc) {
            gc.setFill(Color.ORANGE);
            gc.fillOval(x * squareWidth + squareWidth/6.0, y * squareWidth + squareWidth/6.0, squareWidth * 2 /3, squareWidth * 2 / 3);
        }

        //when eaten, its coordinates change to somewhere random and unoccupied
        //already guaranteed to not spawn on any of the boundaries
        void newSpawn() {
            //pick a coord from the list of unoccupied tiles
            int tile = (int) (Math.random() * emptyTiles.size());
            Coords spawnPoint = emptyTiles.get(tile);
            x = spawnPoint.x;
            y = spawnPoint.y;
        }

        boolean checkSnakeCollision() {
            return (x == snake.getX() && y == snake.getY());
        }
    }

    //list of unoccupied tiles, used for finding a place for the fruit to spawn
    ArrayList<Coords> emptyTiles = new ArrayList<>();
    ArrayList<Wall> walls = new ArrayList<>();
    Fruit fruit;
    public Snake snake = new Snake();
    int level;
    boolean timeLimited = true;
    double timer;
    public int score = 0;
    GraphicsContext gc = getGraphicsContext2D();
    //if the snake makes 2 turns in one frame, it'll invert direction, automatically self-colliding
    //  therefore we allow it to turn only once per frame
    boolean justTurned = false;
    //param is the current level, which will decide how things are generated
    public GameStage(int l) {
        super(750, 825);
        //set up loops for animation and gameplay

        level = l;
        //generate list of empty tiles, represented by their coordinates
        //playable area is between 1 and 13 on both axes
        for (int i = 1; i < 14; i++) {
            for (int j = 1; j < 14; j++) {
                emptyTiles.add(new Coords(i, j));
            }
        }
        //remove tiles occupied by snake
        emptyTiles.removeIf(it -> (it.x == 3 || it.x == 2) && it.y == 7);
        //different configurations of walls and fruits depending on level number
        if (l == 1) {
            addWall(5, 5, 2, 2);
            addWall(9, 7, 2, 2);
            fruit = new Fruit(5, 8);
            timer = 30;
        } else if (l == 2) {
            snake.speed = 0.125;
            addWall(3, 3, 1, 3);
            addWall(10, 6, 2, 1);
            addWall(7, 9, 2, 4);
            fruit = new Fruit(13, 9);
            timer = 30;
        } else {
            snake.speed = 0.1;
            addWall(5, 4, 1, 1);
            addWall(8, 12, 1, 2);
            addWall(1, 9, 2, 1);
            addWall(9, 5, 2, 2);
            fruit = new Fruit(12, 11);
            timer = 0;
            timeLimited = false;
        }
        //set boundaries of the screen
        walls.add(new Wall(0,0, 15, 1));
        walls.add(new Wall(0,14, 15, 1));
        walls.add(new Wall(0,0, 1, 15));
        walls.add(new Wall(14,0, 1, 15));
    }

    //add a wall to the level, also remove the tiles from the list of unoccupied tiles
    void addWall(int topLeftX, int topLeftY, int width, int height) {
        walls.add(new Wall(topLeftX, topLeftY, width, height));
        emptyTiles.removeIf(it -> it.x >= topLeftX && it.x < topLeftX + width
        && it.y >= topLeftY && it.y < topLeftY + height);
    }

    //write the score at the top of the screen
    void drawScore() {
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 750, 750, 75);
        gc.setFill(Color.ORANGE);
        gc.fillOval(325, 762, squareWidth, squareWidth);
        gc.setFill(Color.BLACK);
        gc.setFont(new Font(70));
        gc.fillText(Integer.toString(score), 387, 812);
    }

    public void draw() {
        drawScore();
        for (int i = 0; i < rowCount; i += 1) {
            for (int j = 0; j < rowCount; j += 1) {
                if ((i + j) % 2 == 0) {
                    gc.setFill(Color.DARKBLUE);
                } else {
                    gc.setFill(Color.BLUE);
                }
                gc.fillRect(i * squareWidth, j * squareWidth, squareWidth, squareWidth);
            }
        }
        for (Wall w: walls) {
            if (w.isActive()) {w.draw(gc);}
        }
        fruit.draw(gc);
        snake.draw(gc);
    }

    //move the snake and see what happens
    //returns whether the player has died
    public boolean updateGameState() {
        //move the snake and adjust the list of empty tiles accordingly
        //a newly created part of the snake will remain still for one cycle as it is instantiated at the location of the previous tail
        if (!snake.tail.isNew) {
            emptyTiles.add(new Coords(snake.tail.x, snake.tail.y));
        }
        snake.move();
        emptyTiles.removeIf(it -> it.x == snake.head.x && it.y == snake.head.y);
        //allow the player to turn again
        justTurned = false;
        //check for collision with anything
        for (Wall w: walls) {
            if (snake.checkCollision(w) && w.isActive()) {return true;}
        }
        if (snake.checkSelfCollision()) {return true;}
        if (fruit.checkSnakeCollision()) {
            score += 1;
            //grow snake
            snake.addPart();
            //spawn fruit in new place
            //but first check if there's any empty tiles left (maybe the snake grew too large)
            if (emptyTiles.size() == 0) return false;
            fruit.newSpawn();
        }
        return false;
    }

    public void takeKeyInput(KeyEvent e) {
        if (!justTurned) {
            if ((e.getCode() == KeyCode.W || e.getCode() == KeyCode.UP) && snake.dir != Snake.Direction.South) {
                snake.dir = Snake.Direction.North;
                justTurned = true;
            } else if ((e.getCode() == KeyCode.A || e.getCode() == KeyCode.LEFT) && snake.dir != Snake.Direction.East) {
                snake.dir = Snake.Direction.West;
                justTurned = true;
            } else if ((e.getCode() == KeyCode.S || e.getCode() == KeyCode.DOWN) && snake.dir != Snake.Direction.North) {
                snake.dir = Snake.Direction.South;
                justTurned = true;
            } else if ((e.getCode() == KeyCode.D || e.getCode() == KeyCode.RIGHT) && snake.dir != Snake.Direction.West) {
                snake.dir = Snake.Direction.East;
                justTurned = true;
            }
        }
    }
}
