JAVA_FX_LIB = "C:\Program Files\javafx-sdk-11.0.2\lib"
MAIN = "leohuang.snake.Main"
CODE_LOC = "src/leohuang/snake"
UI_LOC = "src/resources"

default:
	javac -sourcepath src $(CODE_LOC)/*.java -d out --module-path $(JAVA_FX_LIB) --add-modules javafx.controls,javafx.fxml
	cp -f $(UI_LOC)/*.png -d out
	cp -f $(UI_LOC)/*.fxml -d out

run: default
	java -cp out --module-path $(JAVA_FX_LIB) --add-modules javafx.controls,javafx.fxml $(MAIN)

clean:
	-@rm out/*.class
	-@rm out/*.png
