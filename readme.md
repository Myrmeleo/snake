# Snake

Leo Huang  
Made with Java SDK 11  
on Microsoft Windows 10 Pro

2 ways of running this (You must use Java 9.X or later):

1. Execute the jar file  (set current directory to compilerout/artifacts)
    a. Enter "make" on the command line. The makefile contains the command written on the line below, to save time typing.  
    b. Enter "java --module-path javafx-lib --add-modules javafx.controls,javafx.fxml -jar snake.jar"  
    -   The folder next to snake.jar named javafx-lib contains the required modules. The contents are jar files you'd normally find in a javafx lib folder.  
        If this folder is moved, you must rewrite the module path to point to a javafx lib folder.   
    
2. Enter "make run" on the command line.  
    -   First you must set the JAVA_FX_LIB variable in the Makefile to point to the place where your javafx lib folder is.  

Structure of the JavaFX application (for personal reference): Main extends Application. On start(), Application is passed a Stage and shows it. Stage sets a Scene. Scene sets a StackPane as its root. Stackpane sets a GameStage as its child. Gamestage, which extends Canvas, uses its GraphicsContext to draw the game.

Application > Stage > Scene > StackPane > Canvas > GraphicsContext
